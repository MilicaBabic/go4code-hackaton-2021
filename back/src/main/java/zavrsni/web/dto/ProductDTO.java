package zavrsni.web.dto;

import java.util.ArrayList;
import java.util.List;

public class ProductDTO {
	
	private Long id;
	private String creationDate;
	private String creationTime;
	private String name;
	private String description;
	private List <String> pictureURLs = new ArrayList<>();
	private Double price;
	private Integer availableQuantity;
	private CategoryDTO category;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	public String getCreationTime() {
		return creationTime;
	}
	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Integer getAvailableQuantity() {
		return availableQuantity;
	}
	public void setAvailableQuantity(Integer availableQuantity) {
		this.availableQuantity = availableQuantity;
	}
	public List<String> getPictureURLs() {
		return pictureURLs;
	}
	public void setPictureURLs(List<String> pictureURLs) {
		this.pictureURLs = pictureURLs;
	}
	public CategoryDTO getCategory() {
		return category;
	}
	public void setCategory(CategoryDTO category) {
		this.category = category;
	}
	
		
	
	
	
	
	
	

}
