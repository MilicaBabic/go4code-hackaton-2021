package zavrsni.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import zavrsni.model.Cart;
import zavrsni.service.CartService;
import zavrsni.support.CartDtoToCart;
import zavrsni.support.CartToCartDto;
import zavrsni.web.dto.CartDTO;

@RestController
@RequestMapping(value = "/api/carts", produces = MediaType.APPLICATION_JSON_VALUE)
public class CartController {
	@Autowired
	private CartService cartService;
	@Autowired
	private CartToCartDto toCartDto;
	@Autowired
	private CartDtoToCart toCart;
	
	
    @GetMapping
    public ResponseEntity<List<CartDTO>> getAll(){

        List<Cart> cs = cartService.findAll();

        return new ResponseEntity<>(toCartDto.convert(cs), HttpStatus.OK);
    }
    
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CartDTO> create(@RequestBody CartDTO dto){

        Cart cart = toCart.convert(dto);
        Cart saved = cartService.save(cart);

        return new ResponseEntity<>(toCartDto.convert(saved), HttpStatus.CREATED);
    }

}
