package zavrsni.web.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import zavrsni.model.Product;
import zavrsni.service.ProductService;
import zavrsni.support.ProductDtoToProduct;
import zavrsni.support.ProductToProductDto;
import zavrsni.web.dto.ProductDTO;

@CrossOrigin("*")
@RestController
@RequestMapping(value = "/api/products", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProductController {
	@Autowired
	private ProductService productService;
	@Autowired
	private ProductToProductDto toProductDto;
	@Autowired
	private ProductDtoToProduct toProduct;
	
	 @GetMapping
	 public ResponseEntity<List<ProductDTO>> getAll(@RequestParam(required = false, value = "") String categoryName){

	     List<Product> ps = productService.findAllProductsForCat(categoryName);

	      return new ResponseEntity<>(toProductDto.convert(ps), HttpStatus.OK);
	  }
	 
	 @GetMapping("/{id}")
		public ResponseEntity<ProductDTO> get(@PathVariable Long id) {
			Optional<Product> p = productService.findOne(id);

			if (p.isPresent()) {
				return new ResponseEntity<>(toProductDto.convert(p.get()), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		}
	 
	 @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
		public ResponseEntity<ProductDTO> create(@Valid @RequestBody ProductDTO dto) {
			Product product = toProduct.convert(dto);
			Product saved = productService.save(product);

			return new ResponseEntity<>(toProductDto.convert(saved), HttpStatus.CREATED);
		}
	 
	 
	 

}
