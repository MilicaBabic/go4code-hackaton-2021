package zavrsni.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import zavrsni.model.Category;
import zavrsni.service.CategoryService;
import zavrsni.support.CategoryToCategoryDto;
import zavrsni.web.dto.CategoryDTO;

@CrossOrigin("*")
@RestController
@RequestMapping(value = "/api/categories", produces = MediaType.APPLICATION_JSON_VALUE)
public class CategoryController {
	
	@Autowired
	private CategoryService categoryService;
	@Autowired
	private CategoryToCategoryDto toCategoryDto;
	
    @GetMapping
    public ResponseEntity<List<CategoryDTO>> getAll(){

        List<Category> kategorije = categoryService.findAll();

        return new ResponseEntity<>(toCategoryDto.convert(kategorije), HttpStatus.OK);
    }

}
