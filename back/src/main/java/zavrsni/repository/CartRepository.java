package zavrsni.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import zavrsni.model.Cart;

@Repository
public interface CartRepository extends JpaRepository<Cart,Long>{

	Cart findOneById(Long id);
	

}
