package zavrsni.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import zavrsni.model.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long>{

	Category findOneById(Long id);

}
