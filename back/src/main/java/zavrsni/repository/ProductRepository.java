package zavrsni.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import zavrsni.model.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

	List<Product> findAllByOrderByCreationDateDesc();

	List<Product> findAllProductsByCategoryIdOrderByCreationDateDesc(Long id);

	Product findAllByName(String productName);

	Product findOneById(Long id);

	List<Product> findAllProductsByCategoryNameIgnoreCaseContainsOrderByCreationDateDesc(String categoryName);

}
