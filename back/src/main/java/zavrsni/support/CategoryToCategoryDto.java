package zavrsni.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import zavrsni.model.Category;
import zavrsni.web.dto.CategoryDTO;

@Component
public class CategoryToCategoryDto implements Converter<Category, CategoryDTO>{

	@Override
	public CategoryDTO convert(Category source) {
		CategoryDTO dto = new CategoryDTO();
		dto.setId(source.getId());
		dto.setName(source.getName());
		dto.setDescription(source.getDescription());
		dto.setPicture(source.getPicture());
		
		return dto;
	} 
//	
	public List<CategoryDTO> convert(List<Category> kategorije) {
        List<CategoryDTO> dtos = new ArrayList<>();

        for (Category k : kategorije) { 
        	CategoryDTO dto = convert(k);
            dtos.add(dto);
        }

        return dtos;
	}
    

	
	
	

}
