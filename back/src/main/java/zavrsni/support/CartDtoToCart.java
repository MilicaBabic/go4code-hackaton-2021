package zavrsni.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import zavrsni.model.Cart;
import zavrsni.service.CartService;
import zavrsni.service.ProductService;
import zavrsni.web.dto.CartDTO;

@Component
public class CartDtoToCart implements Converter<CartDTO, Cart>{
	
	 @Autowired
	    private CartService cartService;
	 @Autowired
	 private ProductService productService;

	    @Override
	    public Cart convert(CartDTO dto) {
	    	Cart entity;

	        if(dto.getId() == null) {
	            entity = new Cart();
	        }else {
	            entity = cartService.findOneById(dto.getId());
	        }

	        if(entity != null) {
	        	entity.setProducts(productService.findAllByName(dto.getProductName()));
	        	entity.setQuantity(dto.getQuantity());   
	    }
	        return entity; 
	    }


}
