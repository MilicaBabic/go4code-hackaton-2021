package zavrsni.support;

import java.time.LocalDate;
import java.time.LocalTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import zavrsni.model.Product;
import zavrsni.service.CategoryService;
import zavrsni.service.ProductService;
import zavrsni.web.dto.ProductDTO;

@Component
public class ProductDtoToProduct implements Converter<ProductDTO, Product>{
	@Autowired
	private ProductService productService;
	@Autowired
	private CategoryService categoryService;
	
	@Override
	public Product convert(ProductDTO dto) {
		Product product = null;
		if (dto.getId() != null) {
			product = productService.findOne(dto.getId()).get();
		}

		if (product == null) {
			product = new Product();
		}
	
		product.setCategory(categoryService.findOneById(dto.getCategory().getId()));
		product.setName(dto.getName());
		product.setDescription(dto.getDescription());
		product.setPrice(dto.getPrice());
		product.setAvailableQuantity(dto.getAvailableQuantity());
		product.setCreationDate(LocalDate.now());
		product.setCreationTime(LocalTime.now());
		product.setPictureURLs(dto.getPictureURLs());

		return product;
	}

}
