package zavrsni.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import zavrsni.model.Product;
import zavrsni.web.dto.ProductDTO;

@Component
public class ProductToProductDto implements Converter<Product, ProductDTO>{
	@Autowired
	private CategoryToCategoryDto toCategoryDto;

	@Override
	public ProductDTO convert(Product source) {
		ProductDTO dto = new ProductDTO();
		dto.setId(source.getId());
		dto.setCreationDate(source.getCreationDate().toString());
		dto.setCreationTime(source.getCreationTime().toString());
		dto.setName(source.getName());
		dto.setDescription(source.getDescription());
		dto.setPictureURLs(source.getPictureURLs());
		dto.setCategory(toCategoryDto.convert(source.getCategory()));
		dto.setPrice(source.getPrice());
		dto.setAvailableQuantity(source.getAvailableQuantity());	
		
		
		return dto;
	}
	
	public List<ProductDTO> convert(List<Product> products) {
        List<ProductDTO> dtos = new ArrayList<>();

        for (Product p : products) {
        	ProductDTO dto = convert(p);
            dtos.add(dto);
        }

        return dtos;
	}

}
