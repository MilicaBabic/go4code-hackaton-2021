package zavrsni.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import zavrsni.model.Cart;
import zavrsni.web.dto.CartDTO;

@Component
public class CartToCartDto implements Converter<Cart, CartDTO>{

	@Override
	public CartDTO convert(Cart source) {
		CartDTO dto = new CartDTO();
		dto.setId(source.getId());
		dto.setProductName(source.getProducts().getName());
		dto.setQuantity(source.getQuantity());
		return dto;
	}
	
	 public List<CartDTO> convert(List<Cart> source){
	        List<CartDTO> dtos = new ArrayList<>();

	        for(Cart a : source) {
	        	CartDTO dto = convert(a);
	        	dtos.add(dto);
	        }

	        return dtos;
	    }

}
