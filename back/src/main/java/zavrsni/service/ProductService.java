package zavrsni.service;

import java.util.List;
import java.util.Optional;

import zavrsni.model.Product;

public interface ProductService {

	List<Product> findAll();

	Optional<Product> findOne(Long id);

	Product save(Product p);

	List<Product> findAllProductsForCat(String categoryName);

	Product findAllByName(String productName);

	Product findOneById(Long id);

}
