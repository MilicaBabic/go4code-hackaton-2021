package zavrsni.service;

import java.util.List;

import zavrsni.model.Category;

public interface CategoryService {

	List<Category> findAll();

	Category findOneById(Long id);

}
