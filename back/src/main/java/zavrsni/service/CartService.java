package zavrsni.service;

import java.util.List;

import zavrsni.model.Cart;

public interface CartService {

	List<Cart> findAll();

	Cart findOneById(Long id);

	Cart save(Cart cart);

}
