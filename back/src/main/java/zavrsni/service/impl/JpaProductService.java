package zavrsni.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import zavrsni.model.Product;
import zavrsni.repository.ProductRepository;
import zavrsni.service.ProductService;

@Service
public class JpaProductService implements ProductService{
	@Autowired
	private ProductRepository productRepository;

	@Override
	public List<Product> findAll() {
		return productRepository.findAllByOrderByCreationDateDesc();
	}

	@Override
	public Optional<Product> findOne(Long id) {
		return productRepository.findById(id);
	}

	@Override
	public Product save(Product p) {
		return productRepository.save(p);
	}

	@Override
	public List<Product> findAllProductsForCat(String categoryName) {
		if(categoryName==null) {
			categoryName="";
		}
		return productRepository.findAllProductsByCategoryNameIgnoreCaseContainsOrderByCreationDateDesc(categoryName);
	}

	@Override
	public Product findAllByName(String productName) {
		return productRepository.findAllByName(productName);
	}

	@Override
	public Product findOneById(Long id) {
		return productRepository.findOneById(id);
	}

}
