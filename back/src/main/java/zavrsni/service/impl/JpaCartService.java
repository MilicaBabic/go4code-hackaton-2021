package zavrsni.service.impl;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import zavrsni.model.Cart;
import zavrsni.model.Product;
import zavrsni.repository.CartRepository;
import zavrsni.service.CartService;
import zavrsni.service.ProductService;

@Service
public class JpaCartService implements CartService{
	@Autowired
	private CartRepository cartRepository;
	@Autowired
	private ProductService productService;

	@Override
	public List<Cart> findAll() {
		return cartRepository.findAll();
	}

	@Override
	public Cart findOneById(Long id) {
		return cartRepository.findOneById(id);
	}

	@Override
	public Cart save(Cart cart) {
		Product product = productService.findOneById(cart.getProducts().getId());
		if(cart.getQuantity()> product.getAvailableQuantity() || product.getAvailableQuantity()< 0) {
			throw new EntityNotFoundException();
		}
		product.setAvailableQuantity(product.getAvailableQuantity()-cart.getQuantity());
		return cartRepository.save(cart);
	}

}
