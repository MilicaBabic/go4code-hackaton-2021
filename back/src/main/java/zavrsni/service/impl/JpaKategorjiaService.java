package zavrsni.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import zavrsni.model.Category;
import zavrsni.repository.CategoryRepository;
import zavrsni.service.CategoryService;

@Service
public class JpaKategorjiaService implements CategoryService {
	@Autowired
	private CategoryRepository categoryRepository;

	@Override
	public List<Category> findAll() {
		return categoryRepository.findAll();
	}

	@Override
	public Category findOneById(Long id) {
		return categoryRepository.findOneById(id);
	}

}
