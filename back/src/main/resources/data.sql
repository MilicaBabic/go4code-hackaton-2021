INSERT INTO `eCommerce`.`category` (`id`, `description`, `name`, `picture`) VALUES ('1', 'opisKat1', 'imeKat1', 'slikaKat1');
INSERT INTO `eCommerce`.`category` (`id`, `description`, `name`, `picture`) VALUES ('2', 'opisKat2', 'imeKat2', 'slikaKat2');
INSERT INTO `eCommerce`.`category` (`id`, `description`, `name`, `picture`) VALUES ('3', 'opisKat3', 'imeKat3', 'slikaKat3');

INSERT INTO `eCommerce`.`product` (`id`, `available_quantity`, `creation_date`, `creation_time`, `description`, `name`, `price`, `category_id`) VALUES ('1', '2', '2020-06-20', '11:00:00', 'opis1', 'ime1', '50', '3');
INSERT INTO `eCommerce`.`product` (`id`, `available_quantity`, `creation_date`, `creation_time`, `description`, `name`, `price`, `category_id`) VALUES ('2', '4', '2019-05-25', '12:00:00', 'opis2', 'ime2', '70', '2');
INSERT INTO `eCommerce`.`product` (`id`, `available_quantity`, `creation_date`, `creation_time`, `description`, `name`, `price`, `category_id`) VALUES ('3', '6', '2015-06-15', '13:00:00', 'opis3', 'ime3', '100', '3');

INSERT INTO `eCommerce`.`cart` (`id`, `quantity`, `products_id`) VALUES ('1', '5', '1');
INSERT INTO `eCommerce`.`cart` (`id`, `quantity`, `products_id`) VALUES ('2', '2', '2');
