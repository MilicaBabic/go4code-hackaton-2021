import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {Product} from "../../model/product.model";

@Component({
  selector: 'tr[app-product-list-item]',
  templateUrl: './product-list-item.component.html',
  styleUrls: ['./product-list-item.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ProductListItemComponent implements OnInit {

  @Input()
  product: Product;

  constructor() { }

  ngOnInit(){
  }

}
