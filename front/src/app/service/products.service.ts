import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient, HttpParams} from "@angular/common/http";
import {Category} from "../model/category.model";
import {Product} from "../model/product.model";

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  private readonly  path = 'http://localhost:8080/api/products'

  constructor( private http: HttpClient) { }





  getProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(`${this.path}`);
  }


  searchProducts(categoryName: string): Observable<Product[]> {
    const params: HttpParams = new HttpParams().append('categoryName', categoryName);
    return this.http.get<Product[]>(this.path, { params });
  }

  postProduct(product: Product): Observable<Product> {
    return this.http.post<Product>(this.path, product);
  }
}
