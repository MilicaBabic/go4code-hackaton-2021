import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Product} from "../model/product.model";
import {Category} from "../model/category.model";

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {

  @Output()
  productAdded: EventEmitter<Product> = new EventEmitter();

  productToAdd: Product;
  @Input()
  categories: Category[];
  showForm: boolean;

  constructor() { }

  ngOnInit() {
    this.resetProductToAdd()

  }

  addProduct() {
    this.productAdded.next(this.productToAdd);
    this.resetProductToAdd();
  }

  resetProductToAdd() {
    this.productToAdd = new Product({
      categories: [],
      availableQuantity: 0,
        creationDate: "",
        creationTime: "",
        description: "",
        name: "",
        picturesURLs: [],
        price: 0
      }

    );
  }
}
