import { Component, OnInit } from '@angular/core';
import {ProductsService} from "../../service/products.service";
import {Product, ProductInterface} from "../../model/product.model";
import {Category} from "../../model/category.model";
import {CategoryService} from "../../service/category.service";

@Component({
  selector: 'app-products-page',
  templateUrl: './products-page.component.html',
  styleUrls: ['./products-page.component.css']
})
export class ProductsPageComponent implements OnInit {

  public products: Product[];
  public categories: Category[];

  constructor(
    private productService: ProductsService,
    private categoryService: CategoryService,
  )
   { }

  ngOnInit() {
    this.loadProducts()
  }


  loadProducts() {
    this.productService.getProducts().subscribe(
      products => this.products = products
    );
    this.loadCategories()
  }

  loadCategories() {
    this.categoryService.getCategories().subscribe(
      categories => this.categories = categories
    );
  }

  search(categoryName: string) {
    this.productService.searchProducts(categoryName).subscribe((products: Product[]) => this.products = products);
  }

  addProduct(product: Product) {
    this.productService.postProduct(product).subscribe(res => this.loadProducts())
  }

}
