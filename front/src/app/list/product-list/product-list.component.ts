import {Component, Input, OnInit} from '@angular/core';
import {ProductInterface} from "../../model/product.model";

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  @Input()
  products: ProductInterface[];

  constructor() { }

  ngOnInit(){
  }

}
