export interface CategoryInterface {
  id?:number;
  name: string;
  description: string;
  picture: string;
}
export class Category implements CategoryInterface {
  public id?: number;
  public name: string;
  public description: string;
  public picture: string;

  constructor(categoryCfg: CategoryInterface) {
    this.id= categoryCfg.id;
    this.name=categoryCfg.name;
    this.description=categoryCfg.description;
    this.picture=categoryCfg.picture;
  }
}
