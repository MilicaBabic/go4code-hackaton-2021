import {Category} from "./category.model";

export interface ProductInterface {
  id?: number;
  name: string;
  description: string;
  creationDate: string;
  creationTime: string;
  picturesURLs: [];
  categories: Category[];
  category?: Category;
  price: number;
  availableQuantity: number;
}
export class Product implements ProductInterface {
  public id?:number;
  public name: string;
  public description: string;
  public creationDate: string;
  public creationTime: string;
  public picturesURLs: [];
  public categories: Category[];
  public category?: Category;
  public price: number;
  public availableQuantity: number;


  constructor(productCfg: ProductInterface) {
    this.id=productCfg.id;
    this.name=productCfg.name;
    this.description=productCfg.description;
    this.creationDate=productCfg.creationDate;
    this.creationTime=productCfg.creationTime;
    this.picturesURLs=productCfg.picturesURLs;
    this.categories=productCfg.categories;
    this.category=productCfg.category;
    this.price=productCfg.price;
    this.availableQuantity=productCfg.availableQuantity;
  }

}
